package com.mastertech.exercicio;

import java.util.ArrayList;

public class Baralho {

    private ArrayList<Carta> cartas;

    public Baralho(ArrayList<Carta> cartas) {
        this.cartas = cartas;
    }

    public Baralho (){}

    public ArrayList<Carta> getCartas() {
        return cartas;
    }

    public void setCartas(ArrayList<Carta> cartas) {
        this.cartas = cartas;
    }

    public void criaBaralho() {

        ArrayList<Carta> listaDeCartas = new ArrayList<>();

        String[] naipes = {"Ouro", "Copas", "Espadas", "Paus"};
        String[] cartas = {"Ás", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"};

        Baralho baralho = new Baralho();

        System.out.println("Criando baralho... \n");
        for(int i = 0; i < 4; i++){
            for (int j = 0; j < 13; j++) {
                Carta carta = new Carta(naipes[i], cartas[j]);
                listaDeCartas.add(carta);
            }
        }
        this.cartas = listaDeCartas;
    }

    public void embaralha() {
        ArrayList<Carta> deckAuxiliar = new ArrayList<>(this.cartas);

        ArrayList<Carta> cartasEmbaralhadas = new ArrayList<>();

        System.out.println("Criando baralho embaralhado...");
        while (deckAuxiliar.size() > 0) {
            int index = (int) (Math.random() * deckAuxiliar.size());
            cartasEmbaralhadas.add(deckAuxiliar.get(index));
            deckAuxiliar.remove(index);
        }
        this.cartas = cartasEmbaralhadas;
    }

}

package com.mastertech.exercicio;

public class Impressora {

    public static void bemVindoAo21(){
        System.out.println("Bem vindo ao 21 do Mateus! Deseja começar? (S/N)");
    }

    public static void imprimeCarta(Carta carta){
        System.out.println("A carta sorteada foi: " + carta.getValor() + " de " + carta.getNaipe());
    }

    public static void imprimeJogada(Integer valorSomado){
        System.out.println(
                "O jogo está em: " + valorSomado +
                "\nDeseja continuar? (S/N)"
        );
    }

    public static void imprimeResultado(Integer valorSomado, Integer highscore){

        if (valorSomado == 21) {
            System.out.println("Parabéns! Você ganhou!" + "\nSeu highscore é: " + highscore
            );

        } else {
            System.out.println(
                    "Não foi dessa vez... Seu resultado foi: " + valorSomado +
                    "\nSeu highscore é: " + highscore
            );
        }
    }
}

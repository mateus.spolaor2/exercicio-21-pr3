package com.mastertech.exercicio;

public class Main {

    public static void main(String[] args) {

        Baralho baralho = new Baralho();

        Mesa mesa = new Mesa();

        baralho.criaBaralho();

        mesa.setOn(true);
        while(mesa.getOn()){
            baralho.embaralha();
            mesa.apresentaJogo(baralho);
        }

    }
}

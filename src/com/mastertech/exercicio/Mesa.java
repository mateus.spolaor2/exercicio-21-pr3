package com.mastertech.exercicio;

import java.util.ArrayList;
import java.util.Scanner;

import static com.mastertech.exercicio.Impressora.imprimeCarta;
import static com.mastertech.exercicio.Impressora.imprimeResultado;

public class Mesa {

    private Integer highscore = 0;
    private Boolean isOn;

    public Integer getHighscore() {
        return highscore;
    }

    public void setHighscore(Integer highscore) {
        this.highscore = highscore;
    }

    public Boolean getOn() {
        return isOn;
    }

    public void setOn(Boolean on) {
        isOn = on;
    }

    public void apresentaJogo(Baralho baralho) {

        Scanner scanner = new Scanner(System.in);

        Impressora.bemVindoAo21();

        String start = scanner.nextLine();
        if(start.equalsIgnoreCase("S")) {
            int resultado = executaJogo(baralho);
        } else {
            System.out.println("Tchau amiguinho!");
            this.setOn(false);
        }
    }

    private void gravaHighscore(int resultado) {
        if(resultado > this.highscore && resultado <= 21){
            this.highscore = resultado;
        }
    }

    private Integer executaJogo(Baralho baralho) {

        boolean rodadaAtiva = true;
        ArrayList<Carta> cartas = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        int indice = 0;
        int valorTotal = 0;
        String continuar = "S";
        cartas = baralho.getCartas();

        while (rodadaAtiva) {

            valorTotal += somaJogada(cartas.get(indice));
            imprimeCarta(cartas.get(indice));

            if(checaJogada(valorTotal)){
                Impressora.imprimeJogada(valorTotal);
                indice++;
                continuar = scanner.nextLine();
                if(continuar.equalsIgnoreCase("N")){
                    rodadaAtiva = false;
                    gravaHighscore(valorTotal);
                    imprimeResultado(valorTotal, this.highscore);
                }
            } else {
                gravaHighscore(valorTotal);
                imprimeResultado(valorTotal, this.highscore);
                rodadaAtiva = false;
            }
        }

        return valorTotal;
    }

    private Boolean checaJogada(Integer valorTotal) {
        if(valorTotal == 21) {
            return false;
        } else if (valorTotal > 21) {
            return false;
        } else {
            return true;
        }
    }

    private Integer somaJogada(Carta carta) {
        if(carta.getValor().equals("K")
            || carta.getValor().equals("Q")
            || carta.getValor().equals("J")){
            return 10;
        } else if (carta.getValor().equals("Ás")) {
            return 1;
        } else {
            return Integer.parseInt(carta.getValor());
        }
    }
}
